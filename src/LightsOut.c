#include <stdlib.h>
#include <curses.h>
#include <string.h>

// Attributes used for drawing interface elements
const chtype ATTR_NORM  = COLOR_PAIR(1) | A_NORMAL;
const chtype ATTR_FOCUS = COLOR_PAIR(2) | A_BOLD;
const chtype ATTR_OFF   = COLOR_PAIR(3) | A_NORMAL;
const chtype ATTR_ON    = COLOR_PAIR(4) | A_BOLD;

// Useful constants
enum {
    ORDER = 5,
    BOX_WIDTH = 8,
    BOX_HEIGHT = 5
};

struct Game {
	int lightStates[ORDER][ORDER];  // On (1) or off (0) states of ligts
	WINDOW* lights[ORDER][ORDER];   // Windows for displaying each light
	int cursorRow;      // Cursor row (initially at center)
	int cursorCol;      // Cursor column (initially at center)
};

struct Offset {
	int hor;
	int ver;
} neighbourhood[] = {
	{0, 0}, {-1, 0}, {1, 0}, {0, -1}, {0, 1}
};
const int neighboursNum = 5;

void toggleLamp(int lightStates[][ORDER], int row, int col) {
	if (row < 0 || row >= ORDER || col < 0 || col >= ORDER) {
		return;
	}
	lightStates[row][col] ^= 1;
}

void toggleNeighbours(struct Game* game, 
					  const struct Offset* neighbourhood,
					  int neighboursNum) {
	int i;
	for (i = 0; i < neighboursNum; i++) {
		toggleLamp(game->lightStates, 
				   game->cursorRow + neighbourhood[i].ver,
				   game->cursorCol + neighbourhood[i].hor);
	}
};

/**
 * @brief Create ORDER x ORDER windows for each light on the board
 * @return TRUE on success, FALSE otherwise
 */
int createWindows(WINDOW* lights[][ORDER]) {
    int row;
    int col;
    for (row = 0; row < ORDER; row++) {
        for (col = 0; col < ORDER; col++) {
            lights[row][col] = newwin(BOX_HEIGHT, BOX_WIDTH,
                                      row*BOX_HEIGHT, col*BOX_WIDTH);
            if (!lights[row][col]) {
                return FALSE;
            }
        }
    }
    return TRUE;
}

/**
 * @brief Draw all light windows on the screen according to their on/off
 *        status and cursor position
 */
void refreshAll(const struct Game* game) {
    int row, col;
    for (row = 0; row < ORDER; row++) {
        for (col = 0; col < ORDER; col++) {
            if (row == game->cursorRow && 
				col == game->cursorCol) {
                wattrset(game->lights[row][col], ATTR_FOCUS);
            } else {
                wattrset(game->lights[row][col], ATTR_NORM);
            }
            box(game->lights[row][col], 0, 0);
            int x, y;
            for (y = 1; y < BOX_HEIGHT-1; y++) {
                for (x = 1; x < BOX_WIDTH-1; x++) {
                    chtype fill = ACS_BLOCK |
                                  (game->lightStates[row][col] ? ATTR_ON : ATTR_OFF);
                    mvwaddch(game->lights[row][col], y, x, fill);
                }
            }
            // Draw the window on internal buffer (PDCurses virtual screen)
            wnoutrefresh(game->lights[row][col]);
        }
    }
    // Output internal buffer to physical terminal screen once for all windows
    doupdate();
}

int main() {
    initscr();
    start_color();
    curs_set(0);
    resize_term(ORDER*BOX_HEIGHT, ORDER*BOX_WIDTH);
    keypad(stdscr, TRUE);   // Turn on processing of non-alphanumeric keys
    refresh();

    init_pair(1, COLOR_BLUE, COLOR_BLACK);      // Normal border
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);    // Cursor border
    init_pair(3, COLOR_BLACK, COLOR_BLACK);     // Light off
    init_pair(4, COLOR_WHITE, COLOR_BLACK);     // Light on

	struct Game game;
	game.cursorRow = ORDER / 2;      // Cursor row (initially at center)
	game.cursorCol = ORDER / 2;      // Cursor column (initially at center)
	memset(game.lightStates, 0, sizeof(game.lightStates));

    if (!createWindows(game.lights)) {
        endwin();
        return EXIT_FAILURE;
    }
    /* TODO: Add initialization of lights state.
     * At the beginning some ligts must be turned on according to
     * predefined or random pattern.
     */
    refreshAll(&game);

    int running = TRUE;
    while (running) {
        int key = getch();
        switch (key) {
            case 033:   // ESCAPE
                running = FALSE; break;
            case 040:   // Space
				toggleNeighbours(&game, neighbourhood, 
								 neighboursNum);
				break;
            case KEY_DOWN:
                game.cursorRow = (game.cursorRow+1) % ORDER; break;
            case KEY_UP:
                game.cursorRow = (game.cursorRow-1+ORDER) % ORDER; break;
            case KEY_RIGHT:
                game.cursorCol = (game.cursorCol+1) % ORDER; break;
            case KEY_LEFT:
                game.cursorCol = (game.cursorCol-1+ORDER) % ORDER; break;
        }
        refreshAll(&game);
        /* TODO: Add solution detection.
         * The game should end automatically when all lights become off.
         */
    }

    endwin();
    return EXIT_SUCCESS;
}
